# Authelia authentication

### Getting started:

We're not going to presume to generate a configuration.yml for you; upon first running the container authelia itself will create on at the designated spot:
`/config/authelia/configuration.yml`
You could edit this in Visual Studio Code and re-run the container.
It might be possible to place the file in position before starting the container for the first time, however I haven't verified this works or that the permissions are happy.

At a bare minimum to use a "test" environment (not recommended for scaling, but may get you by).

```YAML
authentication_backend:
  file:
    path: /config/authelia/users_database.yml

session:
  local:
    path: /config/authelia/db.sqlite3

notifier:
  filesystem:
    filename: /config/authelia/notification.txt
```