#!/usr/bin/with-contenv bashio

echo "Hello world!"
echo $(pwd)
ls -lh ./target/release/
ls -lh /app/
ls -lh /app/app/
ls -lh /app/app/pkg

mkdir -p /data
if [ ! -f /data/config.toml ]
then
    echo "Installing config template."
    cp -R -u -p /app/lldap_config.docker_template.toml /data/config.toml
fi

/app/target/release/lldap run --config-file /data/config.toml