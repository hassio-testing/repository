#!/bin/sh
# Test stuff
ls -lh /config
ls -lh /app

# Normal stuff
conf_directory="/config/authelia"

if [ ! -d ${conf_directory} ]
then
    mkdir -p ${conf_directory}
fi

exec authelia --config ${conf_directory}/configuration.yml